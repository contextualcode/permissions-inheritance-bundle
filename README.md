# Permissions Inheritance Bundle
eZ Platform bundle which allows to inherit user roles in legacy admin.

## Installation
- Run `composer require`:

```bash
$ composer require contextualcode/permissions-inheritance-bundle
```
- Enable this bundle in `ezpublish/EzPublishKernel.php` file by adding next line in `registerBundles` method:

```php
    public function registerBundles()
    {
        $bundles = array(
            ...
            new ContextualCode\PermissionsInheritanceBundle\ContextualCodePermissionsInheritanceBundle()
        );
```
- Create new custom database tables:

```
$ cat vendor/contextualcode/permissions-inheritance-bundle/src/ContextualCode/PermissionsInheritanceBundle/ezpublish_legacy/cc_permissions_inheritance/sql/mysql/schema.sql | mysql -u <username -p<password> <database_name>
```
- Installs legacy extensions:

```
$ php ezpublish/console ezpublish:legacybundles:install_extensions --relative
```
- Regenerate eZ Publish Legacy autoloads:

```
$ php ezpublish/console ezpublish:legacy:script bin/php/ezpgenerateautoloads.php
```

- Done.

## Usage
1. Just open role view page in legacy admin. There will be new functionality to inherit the roles.
2. Additional code need to be executed to assign inherited roles to the user. You can use custom login handler/event listener or any other way to call that code:
```
ccRoleInheritance::handleUserInheritedRoles(<user_id>);
```
