DROP TABLE IF EXISTS `cc_roles_inheritance`;
CREATE TABLE `cc_roles_inheritance` (
  `parent_role_id` int(11) unsigned NOT NULL,
  `inherit_role_id` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`parent_role_id`, `inherit_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `cc_user_roles_inheritance`;
CREATE TABLE `cc_user_roles_inheritance` (
  `user_id` int(11) unsigned NOT NULL,
  `inherit_role_id` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`user_id`, `inherit_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
