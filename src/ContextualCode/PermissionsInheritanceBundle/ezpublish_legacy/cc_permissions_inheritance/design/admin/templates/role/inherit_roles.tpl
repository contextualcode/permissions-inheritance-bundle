{def
    $inhert_roles = fetch('permissions_inheritance', 'inheritances', hash('role_id', $role.id))
    $parents = fetch('permissions_inheritance', 'parents', hash('role_id', $role.id))
}

<div class="context-attributes"><div class="block">
  <fieldset>
    <legend>{'Roles inherited by the <%role_name> role (%roles_count)'|i18n( 'design/admin/role/view',, hash( '%role_name', $role.name, '%roles_count', $inhert_roles|count ) )|wash}</legend>
    {if $inhert_roles|count|gt(0)}
      <table class="list" cellspacing="0">
        <tr>
          <th>{'Role'|i18n( 'design/admin/role/view' )}</th>
          <th class="tight">&nbsp;</th>
        </tr>
        {foreach $inhert_roles as $_role}
        <tr>
          <td>
            <a href={concat( '/role/view/', $_role.id)|ezurl}>{$_role.name|wash}</a>
          </td>
          <td class="tight">
            <a href={concat( '/permissions_inheritance/remove/', $role.id, '/', $_role.id)|ezurl}>
              <img src={"trash.gif"|ezimage} alt="{'Remove inherit role.'|i18n( 'design/admin/role/view' )}" />
            </a>
          </td>
        </tr>
        {/foreach}
      </table>
    {else}
      <p>{'This role does not inherit any other roles.'|i18n( 'design/admin/role/view' )}</p>
    {/if}
  </fieldset>
</div></div>

{def $roles_can_iherit = fetch('permissions_inheritance', 'roles_which_can_inherit', hash('role_id', $role.id))}
{if $roles_can_iherit|count|gt(0)}
<div class="controlbar"><div class="block">
  <input type="hidden" name="Inherit_ParentRoleID[]" value="{$role.id}" />
  <select name="Inherit_InheritRoleID[]" title="{'Select role.'|i18n( 'design/admin/role/view' )}" multiple="multiple" size="10">
    {foreach $roles_can_iherit as $_role}
      <option value="{$_role.id}">{$_role.name}</option>
    {/foreach}
  </select>
  <input class="button" type="submit" name="AddNewInheritRoleButton" value="{'Add New Inherited Role'|i18n( 'design/admin/role/view' )}" title="{'Add New Inherited Role'|i18n( 'design/admin/role/view' )}" />
</div></div>
{/if}

<div class="context-attributes"><div class="block">
  <fieldset>
    <legend>{'Roles that inherit the <%role_name> role (%roles_count)'|i18n( 'design/admin/role/view',, hash( '%role_name', $role.name, '%roles_count', $parents|count ) )|wash}</legend>
    {if $parents|count|gt(0)}
      <table class="list" cellspacing="0">
        <tr>
          <th>{'Role'|i18n( 'design/admin/role/view' )}</th>
          <th class="tight">&nbsp;</th>
        </tr>
        {foreach $parents as $_role}
        <tr>
          <td>
            <a href={concat( '/role/view/', $_role.id)|ezurl}>{$_role.name|wash}</a>
          </td>
          <td class="tight">
            <a href={concat( '/permissions_inheritance/remove/', $_role.id, '/', $role.id)|ezurl}>
              <img src={"trash.gif"|ezimage} alt="{'Remove inherit role.'|i18n( 'design/admin/role/view' )}" />
            </a>
          </td>
        </tr>
        {/foreach}
      </table>
    {else}
      <p>{'This role does not has any parent roles.'|i18n( 'design/admin/role/view' )}</p>
    {/if}
  </fieldset>
</div></div>

{def $roles_can_be_parent = fetch('permissions_inheritance', 'roles_which_can_be_parent', hash('role_id', $role.id))}
{if $roles_can_be_parent|count|gt(0)}
<div class="controlbar"><div class="block">
  <input type="hidden" name="Parent_InheritRoleID[]" value="{$role.id}" />
  <select name="Parent_ParentRoleID[]" title="{'Select role.'|i18n( 'design/admin/role/view' )}" multiple="multiple" size="10">
    {foreach $roles_can_be_parent as $_role}
      <option value="{$_role.id}">{$_role.name}</option>
    {/foreach}
  </select>
  <input class="button" type="submit" name="AddNewParentRoleButton" value="{'Add New Parent Roles'|i18n( 'design/admin/role/view' )}" title="{'Add New Parent Roles'|i18n( 'design/admin/role/view' )}" />
</div></div>
{/if}

<input type="hidden" name="RedirectRoleID" value="{$role.id}" />
