<?php
/**
 * @package contextualcode/permissions-inheritance-bundle
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    20 July 2018
 * */

class ccRoleInheritance extends eZPersistentObject {

    public static function definition() {
        return array(
            'fields'              => array(
                'parent_role_id'  => array(
                    'name'     => 'ParentRoleID',
                    'datatype' => 'integer',
                    'default'  => 0,
                    'required' => true
                ),
                'inherit_role_id' => array(
                    'name'     => 'InheritRoleID',
                    'datatype' => 'integer',
                    'default'  => 0,
                    'required' => true
                )
            ),
            'function_attributes' => array(),
            'keys'                => array( 'parent_role_id', 'inherit_role_id' ),
            'sort'                => array( 'parent_role_id' => 'desc' ),
            'class_name'          => __CLASS__,
            'name'                => 'cc_roles_inheritance'
        );
    }

    public static function fetch($parentRoleId, $inheritRoleId) {
        $filters = array(
            'parent_role_id' => $parentRoleId,
            'inherit_role_id' => $inheritRoleId
        );
        return eZPersistentObject::fetchObject(self::definition(), null, $filters, true);
    }

    public static function fetchList($conditions = null, $limitations = null, $custom_conds = null) {
        return eZPersistentObject::fetchObjectList(
            static::definition(), null, $conditions, null, $limitations, true, false, null, null, $custom_conds
        );
    }

    public static function fetchAllInheritances($parentRoleId) {
        return self::fetchList(array('parent_role_id' => $parentRoleId));
    }

    public static function fetchAllParents($parentRoleId) {
        return self::fetchList(array('inherit_role_id' => $parentRoleId));
    }

    public static function redirectToAddNewInheritRoleView($uri) {
        if (strpos($uri->attribute('original_uri'), 'role/view/') === false) {
            return;
        }

        $http = eZHTTPTool::instance();
        if ($http->hasPostVariable('AddNewInheritRoleButton')) {
            $parentRoleIDs = (array) $http->postVariable('Inherit_ParentRoleID');
            $inheritRoleIDs = (array) $http->postVariable('Inherit_InheritRoleID');
        }
        if ($http->hasPostVariable('AddNewParentRoleButton')) {
            $parentRoleIDs = (array) $http->postVariable('Parent_ParentRoleID');
            $inheritRoleIDs = (array) $http->postVariable('Parent_InheritRoleID');
        }

        if (count($parentRoleIDs) > 0 && count($inheritRoleIDs) > 0) {
            $http->redirect('/permissions_inheritance/add/' . implode(',', $parentRoleIDs) . '/' . implode(',', $inheritRoleIDs) . '/' . $http->postVariable('RedirectRoleID'));
            eZExecution::cleanExit();
        }
    }

    public static function handleUserInheritedRoles($userId) {
        eZUser::purgeUserCacheByUserId($userId);
        $user = eZUser::fetch($userId);
        if ($user instanceof eZUser === false) {
            return;
        }

        $roleIdsToAdd = self::getRoleIdsToAdd($user);
        self::addInheritRoles($user, $roleIdsToAdd);
        self::removeExpiredInheritRoles($user, $roleIdsToAdd);
    }

    protected static function getRoleIdsToAdd(eZUser $user) {
        $roleIdsToAdd = array();
        $assignedRoleIds = $user->attribute('role_id_list');
        foreach ($assignedRoleIds as $roleId) {
            $inheritances = self::fetchAllInheritances($roleId);
            foreach ($inheritances as $inheritance) {
                $roleIdsToAdd[] = $inheritance->attribute('inherit_role_id');
            }
        }
        $roleIdsToAdd = array_unique($roleIdsToAdd);

        return $roleIdsToAdd;
    }

    protected static function addInheritRoles(eZUser $user, array $roleIdsToAdd) {
        $assignedRoleIds = $user->attribute('role_id_list');

        foreach ($roleIdsToAdd as $roleIdToAdd) {
            if (in_array($roleIdToAdd, $assignedRoleIds)) {
                continue;
            }

            $role = eZRole::fetch($roleIdToAdd);
            if ($role instanceof eZRole) {
                $role->assignToUser($user->attribute('contentobject_id'));

                $tmp = ccUserRoleInheritance::fetch($user->attribute('contentobject_id'), $roleIdToAdd);
                if ($tmp instanceof ccUserRoleInheritance === false) {
                    $userRoleInheritance = new ccUserRoleInheritance();
                    $userRoleInheritance->setAttribute('user_id', $user->attribute('contentobject_id'));
                    $userRoleInheritance->setAttribute('inherit_role_id', $roleIdToAdd);
                    $userRoleInheritance->store();
                }
            }
        }
    }

    protected static function removeExpiredInheritRoles(eZUser $user, array $roleIdsToAdd) {
        $existingUserRoleInheritances = ccUserRoleInheritance::fetchForUser($user->attribute('contentobject_id'));
        foreach ($existingUserRoleInheritances as $userRoleInheritances) {
            if (in_array($userRoleInheritances->attribute('inherit_role_id'), $roleIdsToAdd)) {
                continue;
            }

            $role = eZRole::fetch($userRoleInheritances->attribute('inherit_role_id'));
            if ($role instanceof eZRole) {
                $role->removeUserAssignment($user->attribute('contentobject_id'));
            }

            $userRoleInheritances->remove();
        }
    }
}
