<?php
/**
 * @package contextualcode/permissions-inheritance-bundle
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    20 July 2018
 * */

class ccPermissionsInheritanceFetchFunctions {

    public function fetchInheritances($roleId) {
        $roles = array();

        $inheritances = ccRoleInheritance::fetchAllInheritances($roleId);
        foreach ($inheritances as $inheritance) {
            $role = eZRole::fetch($inheritance->attribute('inherit_role_id'));
            if ($role instanceof eZRole) {
                $roles[] = $role;
            }
        }

        return array('result' => $roles);
    }

    public function fetchRolesWhichCanInherit($roleId) {
        $roles = array();

        $allRoles = eZRole::fetchList(false);
        $tmp = self::fetchInheritances($roleId);
        $inheritances = $tmp['result'];
        foreach ($allRoles as $role) {
            $doInherit = false;
            foreach ($inheritances as $inheritanceRole) {
                if ($role->attribute('id') === $inheritanceRole->attribute('id')) {
                    $doInherit = true;
                    break;
                }
            }

            if ($doInherit === false) {
                $roles[] = $role;
            }
        }

        return array('result' => $roles);
    }

    public function fetchParents($roleId) {
        $roles = array();

        $parents = ccRoleInheritance::fetchAllParents($roleId);
        foreach ($parents as $parent) {
            $role = eZRole::fetch($parent->attribute('parent_role_id'));
            if ($role instanceof eZRole) {
                $roles[] = $role;
            }
        }

        return array('result' => $roles);
    }

    public function fetchRolesWhichCanBeParent($roleId) {
        $roles = array();

        $allRoles = eZRole::fetchList(false);
        $tmp = self::fetchParents($roleId);
        $parents = $tmp['result'];
        foreach ($allRoles as $role) {
            $isParent = false;
            foreach ($parents as $parentRole) {
                if ($role->attribute('id') === $parentRole->attribute('id')) {
                    $isParent = true;
                    break;
                }
            }

            if ($isParent === false) {
                $roles[] = $role;
            }
        }

        return array('result' => $roles);
    }
}
