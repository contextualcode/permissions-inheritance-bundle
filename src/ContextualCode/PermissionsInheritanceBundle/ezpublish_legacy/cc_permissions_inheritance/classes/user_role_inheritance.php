<?php
/**
 * @package contextualcode/permissions-inheritance-bundle
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    20 July 2018
 * */

class ccUserRoleInheritance extends eZPersistentObject {

    public static function definition() {
        return array(
            'fields'              => array(
                'user_id'  => array(
                    'name'     => 'UserID',
                    'datatype' => 'integer',
                    'default'  => 0,
                    'required' => true
                ),
                'inherit_role_id' => array(
                    'name'     => 'InheritRoleID',
                    'datatype' => 'integer',
                    'default'  => 0,
                    'required' => true
                )
            ),
            'function_attributes' => array(),
            'keys'                => array( 'user_id', 'inherit_role_id' ),
            'sort'                => array( 'user_id' => 'desc' ),
            'class_name'          => __CLASS__,
            'name'                => 'cc_user_roles_inheritance'
        );
    }

    public static function fetch($userId, $inheritRoleId) {
        $filters = array(
            'user_id' => $userId,
            'inherit_role_id' => $inheritRoleId
        );
        return eZPersistentObject::fetchObject(self::definition(), null, $filters, true);
    }

    public static function fetchList($conditions = null, $limitations = null, $custom_conds = null) {
        return eZPersistentObject::fetchObjectList(
            static::definition(), null, $conditions, null, $limitations, true, false, null, null, $custom_conds
        );
    }

    public static function fetchForUser($userId) {
        return self::fetchList(array('user_id' => $userId));
    }
}
